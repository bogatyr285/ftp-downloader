const path = require('path');
const fs = require('fs');
const Client = require('ftp');
const options = {
    host: 'localhost' || argv.host,
    port: '21' || argv.port,
    user: 'user' || argv.user,
    password: 'pass' || argv.password,
}
const c = new Client();

const LastDate = new Date(argv.fromdate);
(async function main() {
    try {
        c.connect(options);
        c.on('ready', async () => {

            let files = await GetFilesList(c, '');
            if (files.length == 0) {
                console.log('Nothing to download');
            }
            console.log(files)
            for (file of files) {
                if (LastDate < new Date(file.date)) {
                    //download file
                    await Download(c, file, 'save/');
                }
            }
            LastDate = new Date();
        });
    } catch (err) {
        console.log(err)
    }
})()


//get list from all server
async function GetFilesList(_c, dir, filelist) {
    var files = await getFilesList(_c, dir);
    filelist = filelist || [];

    for (file of files) {
        if (file.type === 'd') {
            filelist = await GetFilesList(_c, dir + file.name + '/', filelist);
        } else {
            file.path = dir + file.name;
            filelist.push(file);
            console.log(`Found file ${file.path}`)
        }
    }
    return filelist;

    //Get list from current dirrectory
    async function getFilesList(_c, dir) {
        return new Promise((resolve, reject) => {
            _c.list(dir, (err, list) => {
                if (err) reject(err);
                resolve(list);
            });
        });
    }
}
async function Download(_c, file, pathToSave) {
    console.log(`[${new Date().toLocaleString()}] Start downloading ${JSON.stringify(file, null, 2)}`);
    return new Promise((resolve, reject) => {
        _c.get(file.path, (err, stream) => {
            if (err) reject(err);

            let folderPath = pathToSave + file.path.slice(0, file.path.lastIndexOf('/'));
            if (!fs.existsSync(folderPath)) {
                mkdirpSync(folderPath);
            }

            let objectSize = file.size;
            let lastProgress = new Date();
            let lastSize = 0;
            stream.on('data', (chunk) => {
                if (lastProgress.getTime() < new Date().getTime() - 10000 / 5) {
                    fs.stat(`${pathToSave}${file.path}`, (err, stat) => {
                        if (err) {
                            console.log("Failed to read FS stat. Got: " + JSON.stringify(err));
                            reject(err);
                        }

                        let progress = (objectSize > 0) ? Math.round(100.0 * (lastSize / objectSize)) : 0;
                        let speed = ((stat.size - lastSize) / 1024) / 2;

                        console.log(`[${new Date().toLocaleString()}] Downloaded ${file.name} ${lastSize}/${objectSize} bytes, progress: ${progress}%, speed ${speed.toFixed(2)} Kb/s`);
                        lastSize = stat.size;
                    });
                    lastProgress = new Date();
                }
            });
            stream.pipe(fs.createWriteStream(`${pathToSave}${file.path}`));
            stream.once('close', function () { console.log(`[${new Date().toLocaleString()}] ${file.path} Downloaded`); resolve(true); });
        });
    });
}

function mkdirpSync(dirPath) {
    const parts = dirPath.split(path.sep)

    for (let i = 1; i <= parts.length; i++) {
        mkdirSync(path.join.apply(null, parts.slice(0, i)))
    }
    function mkdirSync(dirPath) {
        try {
            fs.mkdirSync(dirPath)
        } catch (err) {
            if (err.code !== 'EEXIST') throw err
        }
    }
}